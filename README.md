# Placement Service

This module was developed using and intended for Roblox Studio

Placement Service is an advanced kit that allows you to create a fully functional sandbox tycoon style placement system without going through the hassle of creating one from scratch yourself! With countless polished and easy to use features, anyone with basic Luau knowledge can make a unique and secure placement system in minutes!

---

## Features

### **Features**
- [x] Accurate movement system (grid/no grid)
- [x] Angle Tilting
- [x] Ability to toggle selection boxes on or off during placement
- [x] Auto anchor
- [x] Auto place system
- [x] Collisions
- [x] Cool down timer for placement
- [x] Cross platform support for PC, XBOX and Mobile
- [x] Custom callback functions on placement
- [x] Customizable colors when placing and colliding
- [x] Custom keybinds for cancelling placement, change floor and rotation
- [x] Custom Range for models to be placed in. Where if the model is too far away from the character you cant place it
- [x] Floors system (able to raise and lower the object)
- [x] Grid fade in/out toggles
- [x] Haptic feedback (XBOX)
- [x] Interpolation (smoothing)
- [x] No Plot Placement
- [x] Optional audible feedback
- [x] Rotation
- [x] Server side placement
- [x] Stackable objects
- [x] Signals/Events system

More coming soon!

---

NOTE: Was originally named "Placement Module V3". This module no longer goes by that name.

This module is the first iteration out of three modules to be object oriented making it even more powerful. It is also the **easiest** published placement system kit on the Roblox platform! 

## Benefits of this module

- **Includes many pre-built features.** 22 Are listed above.
- **Is easy to learn and use.** The module has been designed with beginner programmers in mind. It only requires beginner scripting to get the system up and working. *It removes the hassle of creating a complex system of your own.*
- **Regularly updated.** The module is always being updated with feature updates as well as bug fixes.

---

Want to use this module in your game/project? Use the resources below to learn how the module works:

- Use the API and sample code included with the module
- [Dev forum tutorial](https://devforum.roblox.com/t/how-to-use-placement-service/698753)
- [Placement Module Wiki](https://zblox164.github.io/PlacementService/)

This module and all of its content was written by [zblox164](https://www.roblox.com/users/60715914/profile). You can get the module in the Roblox library [here](https://www.roblox.com/library/5073110873/Placement-Service).

Module Version: 1.5.7
