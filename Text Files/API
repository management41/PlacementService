--[[

API:


FUNCTIONS

placementInfo MODULE.new(
	int grid,
	obj objectLocation,
	Enum rotateInput, Enum terminateInput, Enum raiseInput, Enum lowerInput,
	Enum xboxRotateInput, Enum xboxTerminateInput, Enum xboxRaisInpute, Enum xboxLowerInput
)
							
void placementInfo:terminate()
Terminates the current placement (if placement is active)

void placementInfo:activate(string objectName, obj location where the model will be placed, obj plane/plot, bool stackable, bool smart rotation, bool autoPlace)
Activates placement

void placementInfo:noPlotActivate(string objectName, obj location where the model will be placed, bool smart rotation, bool autoPlace)
Activates a non plot dependant placement

void placementInfo:requestPlacement(Instance RemoteFunction, function callback)
Requests a server sided placement. Callback is not required

void placementInfo:haltPlacement() - Stops automatic placement (autoPlace must be true)
Freezes the current placement if autoPlace is set to true

void placementInfo:pauseCurrentState()
Pauses the current state of placement

void placementInfo:resume()
Resumes the current state of placement

void placementInfo:editAttribute(string attributeName, var input)
Edits the inputted attribute (if avaliable) value with the input value

void placementInfo:rotate()
Manually rotates the object (used for mobile support)

void placementInfo:raise()
Manually raises the objects "floor" (used for mobile support)

void placementInfo:lower()
Manually lowers the objects "floor" (used for mobile support)

string placementInfo:getCurrentState()
Returns the current state of placement

string placementInfo:getPlatform()
Returns the current platform the user is using


EVENTS

void placementInfo.Activated

void placementInfo.Placed

void placementInfo.Rotated

void placementInfo.Terminated

obj placementInfo.Collided

bool placementInfo.ChangedFloors


CODE:

-- Client --

local itemPlacement = require(game.ReplicatedStorage.location.PlacementModuleV3)

local remote = game.ReplicatedStorage.location.requestPlacement

local player = game.Players.LocalPlayer
local mouse = player:GetMouse()

local plot = plotLocation

local button = buttonLocation

local placementInfo = itemPlacement.new(
	2,
	game.ReplicatedStorage.location,
	Enum.KeyCode.R, Enum.KeyCode.X, Enum.KeyCode.U, Enum.KeyCode.L,
	Enum.KeyCode.ButtonB, Enum.KeyCode.ButtonA, Enum.KeyCode.ButtonX, Enum.KeyCode.ButtonY
)

button.MouseButton1Click:Connect(function()
	itemPlacement:activate("Wall", plot.PlacedObjectsLocation, plot.PlotLocation, true, true, true)
end)

local function optionalCallback()
	print("Object has been placed")
end

mouse.Button1Down:Connect(function()
	itemPlacement:requestPlacement(remote, optionalCallback)
end)

-- If you have autoplace enabled
mouse.Button1Up:Connect(function()
	itemPlacement:haltPlacement()
end)

-- Server --

local replicatedStorage = game:GetService("ReplicatedStorage")

-- Ignore the top three functions

-- Credit EgoMoose
local function checkHitbox(character, object)
	if object then
		local collided = false

		local collisionPoint = object.PrimaryPart.Touched:Connect(function() end)
		local collisionPoints = object.PrimaryPart:GetTouchingParts()

		for i = 1, #collisionPoints do
			if not collisionPoints[i]:IsDescendantOf(object) and not collisionPoints[i]:IsDescendantOf(character) then
				collided = true

				break
			end
		end

		collisionPoint:Disconnect()

		return collided
	end
end

local function checkBoundaries(plot, primary)
	local lowerXBound
	local upperXBound

	local lowerZBound
	local upperZBound

	local currentPos = primary.Position

	lowerXBound = plot.Position.X - (plot.Size.X*0.5) 
	upperXBound = plot.Position.X + (plot.Size.X*0.5)

	lowerZBound = plot.Position.Z - (plot.Size.Z*0.5)	
	upperZBound = plot.Position.Z + (plot.Size.Z*0.5)

	return currentPos.X > upperXBound or currentPos.X < lowerXBound or currentPos.Z > upperZBound or currentPos.Z < lowerZBound
end

local function handleCollisions(char, item, c)
	if c then
		if not checkHitbox(char, item) then
			item.PrimaryPart.Transparency = 1

			return true
		else
			item:Destroy()

			return false
		end
	else
		item.PrimaryPart.Transparency = 1

		return true
	end
end

--Ignore above

local function place(plr, name, location, prefabs, cframe, c, plot)
	local item = prefabs:FindFirstChild(name):Clone()
	item.PrimaryPart.CanCollide = false
	item:PivotTo(cframe)
	
	if plot then
		if checkBoundaries(plot, item.PrimaryPart) then
			return
		end

		item.Parent = location

		return handleCollisions(plr.Character, item, c)
	else
		return handleCollisions(plr.Character, item, c)
	end
end

replicatedStorage.REMOTE_FUNCTION_LOCATION.OnServerInvoke = place

]]

-- Created and written by zblox164 (2020-2022)
