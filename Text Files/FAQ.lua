--[[

Thank you for using Placement Service!

FAQ:
	Q. Wasn't this used to be named "Placement Module V3"?
	A. Yes! The module was changed from "Placement Module V3" to "Placement Service"
	   since "Placement Module V3" isn't easy remember and tell what it is without
	   using the module prior.
	
	Q. It is broken. How do I fix this? 
	A. The most likely reason for this, is that you have incorrectly setup the module. 
	   You will have to fix your code to fix this 99% of the time. You can also try checking 
	   for a newer version of the module.
	
	Q. How do I save/load data. 
	A. This module does NOT handle any other tycoon features than the placement system 
	   itself.
	
	Q. How do I create an inventory system? 
	A. Again, this is NOT included in the module itself. You will have to research this 
	   topic and implement this feature on your own.

	Q. How would I add a bloxburg style placement system to your model? 
	A. The module does unfortunately NOT handle this feature. This is merely just a 
	   standard placement system where you can move and place objects down. You will have to 
	   implement this feature on your own.
	
	Q. Is this module cross platform?
	A. Yes! This module supports XBOX, PC and Mobile devices!
	
	Q. Do I have to credit you for using this module?
	A. No, however you cannot claim it as your own.

If you are interested in learning this module, here are some resources you can use:
 * Look in the API script (sample code is included there)
 * My dev forum tutorial - https://devforum.roblox.com/t/how-to-use-my-placement-module-v3/698753/1
 * Official Github repository - https://github.com/zblox164/PlacementModuleV3

]]

-- Created and written by zblox164 (2020-2022)
